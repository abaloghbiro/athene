package hu.braininghub.bh06.athene.model;

import hu.braininghub.bh06.athene.model.calendar.Semester;

public class SchoolYear {

	private Semester firstSemester;
	private Semester secondSemester;
	private String identifer;
	
	public SchoolYear(String identifier, Semester firstSemester, Semester secondSemester) {
		super();
		this.firstSemester = firstSemester;
		this.secondSemester = secondSemester;
		this.identifer = identifier;
	}

	public Semester getFirstSemester() {
		return firstSemester;
	}

	public Semester getSecondSemester() {
		return secondSemester;
	}

	public String getIdentifer() {
		return identifer;
	}
	
	
}
