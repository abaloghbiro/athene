package hu.braininghub.bh06.athene.model;

import java.time.LocalDateTime;

import hu.braininghub.bh06.athene.model.calendar.Semester;

public class SignUp {

	private Subject subject;
	private Semester semester;
	private Student student;
	private LocalDateTime signUpTime;

	public SignUp(Subject subject, Semester semester, Student student) {
		this.subject = subject;
		this.semester = semester;
		this.student = student;
		this.signUpTime = LocalDateTime.now();
	}

	public Subject getSubject() {
		return subject;
	}

	public Semester getSemester() {
		return semester;
	}

	public Student getStudent() {
		return student;
	}

	public LocalDateTime getSignUpTime() {
		return signUpTime;
	}

}
