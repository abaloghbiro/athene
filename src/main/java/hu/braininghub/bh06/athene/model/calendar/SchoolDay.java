package hu.braininghub.bh06.athene.model.calendar;

import java.time.LocalDate;
import java.util.List;

public class SchoolDay {

	private LocalDate schoolDayDate;
	private int dayStartHour;
	private int dayFinishHour;
	private List<SchoolLesson> lessions;

	public LocalDate getSchoolDayDate() {
		return schoolDayDate;
	}

	public void setSchoolDayDate(LocalDate schoolDayDate) {
		this.schoolDayDate = schoolDayDate;
	}

	public List<SchoolLesson> getLessions() {
		return lessions;
	}

	public void setLessions(List<SchoolLesson> lessions) {
		this.lessions = lessions;
	}

	public int getDayStartHour() {
		return dayStartHour;
	}

	public void setDayStartHour(int dayStartHour) {
		this.dayStartHour = dayStartHour;
	}

	public int getDayFinishHour() {
		return dayFinishHour;
	}

	public void setDayFinishHour(int dayFinishHour) {
		this.dayFinishHour = dayFinishHour;
	}

}
