package hu.braininghub.bh06.athene.model.calendar;

import java.time.LocalDate;
import java.util.List;

public class Semester {

	private LocalDate startDateOfSemester;
	private LocalDate endDateOfSemester;
	private List<SchoolWeek> weeks;

	public LocalDate getStartDateOfSemester() {
		return startDateOfSemester;
	}

	public void setStartDateOfSemester(LocalDate startDateOfSemester) {
		this.startDateOfSemester = startDateOfSemester;
	}

	public LocalDate getEndDateOfSemester() {
		return endDateOfSemester;
	}

	public void setEndDateOfSemester(LocalDate endDateOfSemester) {
		this.endDateOfSemester = endDateOfSemester;
	}

	public List<SchoolWeek> getWeeks() {
		return weeks;
	}

	public void setWeeks(List<SchoolWeek> weeks) {
		this.weeks = weeks;
	}

}
