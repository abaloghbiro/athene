package hu.braininghub.bh06.athene.model;

import java.time.LocalDate;
import java.util.List;

public class Student {

	private String firstname;
	private String lastname;
	private LocalDate birthDate;
	private String identifier;
	private List<SignUp> currentSubjects;

	public Student(String firstname, String lastname, LocalDate birthDate, String identifier) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthDate = birthDate;
		this.identifier = identifier;
	}

	public List<SignUp> getCurrentSubjects() {
		return currentSubjects;
	}

	public void setCurrentSubjects(List<SignUp> currentSubjects) {
		this.currentSubjects = currentSubjects;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public String getIdentifier() {
		return identifier;
	}

}
