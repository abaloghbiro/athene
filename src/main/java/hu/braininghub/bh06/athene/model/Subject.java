package hu.braininghub.bh06.athene.model;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private List<Subject> prerequisites = new ArrayList<Subject>();
	private AssessmentType assessmentType;
	private String subjectName;
	private String subjectCode;
	private String subjectDescription;
	
	public Subject(List<Subject> prerequisites, AssessmentType assessmentType, String subjectName, String subjectCode,
			String subjectDescription) {
		super();
		this.prerequisites = prerequisites;
		this.assessmentType = assessmentType;
		this.subjectName = subjectName;
		this.subjectCode = subjectCode;
		this.subjectDescription = subjectDescription;
	}

	public String getSubjectDescription() {
		return subjectDescription;
	}

	public void setSubjectDescription(String subjectDescription) {
		this.subjectDescription = subjectDescription;
	}

	public List<Subject> getPrerequisites() {
		return prerequisites;
	}

	public AssessmentType getAssessmentType() {
		return assessmentType;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public String getSubjectCode() {
		return subjectCode;
	}
	
	
	
}
