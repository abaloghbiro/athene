package hu.braininghub.bh06.athene.model;

public enum AssessmentType {
	EXAM, SIGNATURE, PRACTICE_MARK;
}
