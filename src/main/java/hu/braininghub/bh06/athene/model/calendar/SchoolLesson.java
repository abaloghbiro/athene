package hu.braininghub.bh06.athene.model.calendar;

import hu.braininghub.bh06.athene.model.Room;
import hu.braininghub.bh06.athene.model.Subject;
import hu.braininghub.bh06.athene.model.Teacher;

public class SchoolLesson {

	private int durationInMin;
	private Subject subject;
	private Teacher teacher;
	private Room room;

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public int getDurationInMin() {
		return durationInMin;
	}

	public void setDurationInMin(int durationInMin) {
		this.durationInMin = durationInMin;
	}

}
