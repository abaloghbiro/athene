package hu.braininghub.bh06.athene.model.calendar;

import java.util.ArrayList;
import java.util.List;

public class SchoolWeek {

	private List<SchoolDay> days = new ArrayList<>(5);

	public List<SchoolDay> getDays() {
		return days;
	}

	public void setDays(List<SchoolDay> days) {
		this.days = days;
	}
	
	
}
